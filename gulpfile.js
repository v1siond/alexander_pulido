/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins

const gulp = require('gulp'),
      gutil = require("gulp-util"),
      webpack = require('webpack'),
      webpackConfig = require('./webpack.config.js'),
      WebpackDevServer = require("webpack-dev-server"),
      sass = require('gulp-sass'),
      bulkSass = require('gulp-sass-bulk-import'),
      autoprefixer = require('gulp-autoprefixer'),
      cssnano = require('gulp-cssnano'),
      imagemin = require('gulp-imagemin'),
      rename = require('gulp-rename'),
      concat = require('gulp-concat'),
      notify = require('gulp-notify'),
      cache = require('gulp-cache')
      del = require('del'),
      livereload = require('gulp-livereload'),
      watch = require('gulp-watch');

const paths = {
    src: 'src/',
    stylesheet: 'app/assets/stylesheet/',
    js: 'app/assets/js',
    images: 'app/assets/images'
};

// Production build
gulp.task('build', ['webpack:build']);

gulp.task('webpack:build', function (callback) {
    webpack(webpackConfig, function (err, stats) {
      if (err)
          throw new gutil.PluginError('webpack:build', err);
      gutil.log('[webpack:build] Completed\n' + stats.toString({
          assets: true,
          chunks: false,
          chunkModules: false,
          colors: true,
          hash: false,
          timings: false,
          version: false
      }));
      callback();
    });
});

gulp.task("webpack-dev-server", function(callback) {

  var compiler = webpack(webpackConfig);

  new WebpackDevServer(compiler, {
  }).listen(8081, "localhost", function(err) {
    if(err) throw new gutil.PluginError("webpack-dev-server", err);
    gutil.log("[webpack-dev-server]", "http://localhost:8081/");
  });
});


// Styles

gulp.task('styles', [], function() {
  gulp.src(paths.src + 'stylesheet/**/*.scss' )
    .pipe( bulkSass() )
    .pipe( sass().on('error', sass.logError) )
    .pipe(autoprefixer('last 2 version'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssnano())
    .pipe(gulp.dest(paths.stylesheet))
    .pipe(notify({ message: 'Styles task complete' }));
});


// Images
gulp.task('images', function() {
  return gulp.src(paths.src + 'images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(paths.images))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('html', function() {
  return gulp.src(paths.src + '*.html')
    .pipe(gulp.dest('./'))
    .pipe(notify({ message: 'Html task complete' }));
});

// Clean
gulp.task('clean', function() {
  return del([paths.stylesheet, paths.js + '/**/*', paths.images + '/**/*']);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('html', 'styles', 'images', 'build');
});

// Development task
gulp.task('development', ['clean', 'watch'], function() {
  gulp.start('html', 'styles', 'images', 'build', 'webpack-dev-server');
});

gulp.task('watch', function(){
  gulp.watch(paths.src + '*.html', ['html']);
  gulp.watch(paths.src + 'js/*.js', ['webpack:build']);
  gulp.watch(paths.src + 'stylesheet/**/*.scss', ['styles']);
  gulp.watch(paths.src + 'images/**/*', ['images']);
});
