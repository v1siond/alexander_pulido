var webpack = require('webpack'),
    glob = require('glob'),
    path = require('path');

var config = {
  context: __dirname,
  entry: glob.sync('./src/**/*.js').reduce((entries, entry) => Object.assign(entries, {[entry.replace('./src/', '').replace('.js', '')]: entry}), {}),

  output: {
    path: __dirname + '/app/assets',
    publicPath: '/app/assets',
    filename: '[name].es6.js',
  },

  //To run development server
  devServer: {
    contentBase: __dirname + '/app/',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: { presets: ['es2015'] }
        }]
      }
    ]
  },

  resolve: {
    extensions: [ '.js' ]
  },

  devtool: "eval-source-map"
};

// Check if build is running in production mode, then change the sourcemap type
if (process.env.NODE_ENV === "production") {
  config.devtool = ""; // No sourcemap for production

  // Add more configuration for production here like
  // Uglify plugin
  // Offline plugin
  // Etc,
}

module.exports = config;