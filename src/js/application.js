import Database from './db/database'
import Login from './components/login'
import Signup from './components/signup'
import Profile from './components/profile'
import Options from './components/options'
import Helpers from './helpers'

const database = new Database('alexander_test'),
      home = new Login(database),
      helpers = new Helpers(),
      signup = new Signup(database),
      profile = new Profile(helpers, database),
      options = new Options(helpers),
      saveUser = () => signup.signup(),
      login = () => home.login(),
      logout = () => profile.logout(),
      edit = () => profile.edit(),
      cancelEdit = () => profile.cancelEdit(),
      updateUser = () => profile.saveEdit(),
      swap = () => helpers.handleSwap();

let oldView = '',
    currentWidth = 'na',
    swapZoneMobile = '.data-edit--mobile',
    swapZoneDesktop = '.data-edit',
    swapOption = '.swap_option',
    active = '.swap_option.active',
    start,
    stop,
    optionIndex;

$(window).on('load hashchange resize', (e) => {
  e.preventDefault;
  let setView,
      size = helpers.getSize();;
  if ($.session.get("user") != '' && $.session.get("user") != null) {
    let view = new Profile(helpers, database);
    if (currentWidth != size) {
      window.history.pushState("", "", '#user/profile');
      oldView = 'user/profile';
      currentWidth = size;
      setView = () => view.show();
      setView();   
    }
  } else {
    let rawView = document.location.hash,
        viewName = rawView.replace(/^#\//, '');

    if (!viewName == oldView || viewName == '' || viewName == 'user/profile') {
      switch(viewName) {
        case 'signup': {
          let view = signup;
          setView = () => view.show();
          break; 
        }
        case 'user/profile': {
          window.location.replace("");
          break; 
        } 
        default: {
          let view = home;
          setView = () => view.show();
          break; 
        }     
      }
      oldView = viewName;
      setView();
    }    
  }

  if ($(window).width() < 961) {
    interact(swapZoneMobile)
      .draggable({
        // enable inertial throwing
        inertia: true,
        // keep the element within the area of it's parent
        axis: "x",
        restrict: {
          restriction: "parent"
        },
        // enable autoScroll
        autoScroll: true,
        onstart: () => {
          start = $(swapZoneMobile).offset().left;
        },
        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: (event) => {
          let stop = $(swapZoneMobile).offset().left;
          if(stop > start){
            optionIndex = $(active).index() + 1;
          } else {
            optionIndex = (($(active).index() > 0) ? $(active).index() - 1 : 0);
          } 
          showOptionView();
        }
      });
  }
});

function dragMoveListener (event) {
  var target = event.target,
      // keep the dragged position in the data-x/data-y attributes
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform =
  target.style.transform =
    'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

let showOptionView = () => {
  $(swapOption).removeClass('active');
  $(swapOption).eq(optionIndex).addClass('active');
  $(swapZoneMobile).children().fadeOut("fast");
  options.getOptionView($(swapOption).eq(optionIndex).attr('name'), '.main-section');
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;
$(document).on('click', '.default-button__login-form', login);
$(document).on('click', '.default-button__signup-form', saveUser);
$(document).on('click', '.ion-edit.-edit', edit);
$(document).on('click', '#cancel_save', cancelEdit);
$(document).on('click', '#save', updateUser);
$(document).on('click', '#logout', logout);
$(document).on('click', swapOption, (e) => {
  e.preventDefault;
  $(swapOption).removeClass('active');
  $(e.target).addClass('active');
  $(swapZoneDesktop).children().fadeOut("fast");
  console.log($(e.target));
  options.getOptionView($(e.target).attr('name'), '.main-section');
});