export default class Helpers {

  getUser() {

    try  {
      let user = JSON.parse($.session.get("user"));
      return user;
    } catch(err) {
      return 'na';
    }
  }

  insertVars(var_strings, vars, view) {
    for (let i=0; i < vars.length; i++) {
      view = view.replace(var_strings[i], vars[i]);
    }
    return view;
  }

  getPath() {
    let userPath = (($(window).width() > 767) ? " src/views/user/desktop/" : "src/views/user/mobile/");
    return userPath;
  }

  getRemote(url) {
    return $.ajax({
        type: "GET",
        url: url,
        async: false
    }).responseText;
  }

  getSize() {
    let currentWidth = (($(window).width() > 767) ? "desktop" : "mobile");
    return currentWidth;
  }
}