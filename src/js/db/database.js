export default class Database {
  /**
   * @param {!string} name Database name
   * @param {function()} [callback] Called when the Database is ready
   */
  constructor(name, callback) {
    /**
     * @type {Storage}
     */
    const localStorage = window.localStorage;

    /**
     * @type {UserList}
     */
    let liveTodos;

    /**
     * Read the local UserList from localStorage.
     *
     * @returns {UserList} Current array of todos
     */
    this.getLocalStorage = () => {
      return liveTodos || JSON.parse(localStorage.getItem(name) || '[]');
    };

    /**
     * Write the local UserList to localStorage.
     *
     * @param {UserList} todos Array of todos to write
     */
    this.setLocalStorage = (todos) => {
      localStorage.setItem(name, JSON.stringify(liveTodos = todos));
    };

    if (callback) {
      callback();
    }
  }

  /**
   * Find users with properties matching those on query.
   *
   * @param {UserQuery} query Query to match
   * @param {function(UserList)} callback Called when the query is done
   *
   * @example
   * db.find({completed: true}, data => {
   *   // data shall contain users whose completed properties are true
   * })
   */
  find(username, password) {
    const todos = this.getLocalStorage();
    let exist = false,
        userData;
    $.each(todos, (index, val) => {
      let user = JSON.parse(val);
      if (username == user.username &&  password == user.password ) {
        userData = JSON.stringify({id: user.id, username: user.username, password: user.password, email: user.email, name: user.name, direction: user.direction, phone: user.phone, webpage: user.webpage, followers: user.followers, reviews: user.reviews});
        exist = true;
      };
    });
    if (exist == false) {
      alert('User not found');
    } else {
      alert('Welcome ' + username);
      $.session.set("user", userData);
      window.location.replace("");
    }
  }

  /**
   * Update an user in the Database.
   *
   * @param {UserUpdate} update Record with an id and a property to update
   * @param {function()} [callback] Called when partialRecord is applied
   */
  update(id, update, fieldName, callback) {
    const todos = this.getLocalStorage();
    let i = todos.length;

    let newData = JSON.parse(update),
    userData;
    $.each(todos, (index, val) => {
      let user = JSON.parse(val);
      if (user.id == id) {
        let keys = Object.keys(user);
        for (var i = 0; i < keys.length; i++) {
          if (keys[i] == fieldName) {
            user[keys[i]] = newData[fieldName];
            alert('Successful Update!');
          } else {
            for (var j = 0; j < fieldName.length; j++) {
              if (keys[i] == fieldName[j]) {
                user[keys[i]] = newData[keys[i]];
              }
            }
          }
        }
        userData = JSON.stringify({id: user.id, username: user.username, password: user.password, email: user.email, name: user.name, direction: user.direction, phone: user.phone, webpage: user.webpage, followers: user.followers, reviews: user.reviews});
      };
    });

    this.setLocalStorage(todos);
    $.session.set("user", userData);

    if (callback) {
      callback();
    } else {
      window.location.replace("");
    }
  }

  /**
   * Insert an user into the Database.
   *
   * @param {User} user User to insert
   * @param {function()} [callback] Called when user is inserted
   */
  insert(user) {
    const todos = this.getLocalStorage();
    let checkUser = JSON.parse(user),
        exist = false;
    $.each(todos, (index, val) => {
      let user = JSON.parse(val);
      if (checkUser.username == user.username || checkUser.email == user.email ) {
        exist = true;
      };
    });
    if (exist == false) {
      todos.push(user);
      alert('Successful Signup!');
    } else {
      alert(checkUser.username + 'User already exist');
    }
    this.setLocalStorage(todos);
    window.location.replace("");
  }
}