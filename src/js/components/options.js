export default class Options {

  constructor(helpers) {
    this.helpers = helpers;
    this.user = this.helpers.getUser();
    this.path = () => this.helpers.getPath();
    this.variablesString = ['nameValue', 'emailValue', 'webpageValue', 'phoneValue', 'directionValue'];
    this.values = ((this.user == 'na') ? '' : [this.user.name, this.user.email, this.user.webpage, this.user.phone, this.user.direction]);
    ;  
  }  

  getOptionView(viewName, container) {
    let markupUrl = this.path() + viewName + '.html',
        view = this.helpers.getRemote(markupUrl);

    view = this.helpers.insertVars(this.variablesString, this.values, view);

    if (view == 'about' && this.helpers.getSize() == 'mobile') {
      $(container).remove();
      return $('.view-wrapper').append(view);
    } else {
      return $(container).html(view);
    }
  }
}