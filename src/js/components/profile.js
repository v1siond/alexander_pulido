import Header from './header'

export default class Profile { 

  constructor(helpers, database) {
    this.database = database;
    this.name = 'profile';
    this.helpers = helpers;
    this.user = this.helpers.getUser();
    this.headerClass = new Header(this.name, this.user, this.helpers);
    this.header = this.headerClass.export();
    this.path = () => this.helpers.getPath();
    this.oldAbout = '';
    this.variablesString = ['nameValue', 'emailValue', 'webpageValue', 'phoneValue', 'directionValue'];
  }

  show() {
    let container = $('.view-wrapper'),
        view,
        markupUrl,
        values = [this.user.name, this.user.email , this.user.webpage, this.user.phone, this.user.direction];

    markupUrl = this.path() + 'about.html';
    view = this.helpers.getRemote(markupUrl);
    view = this.helpers.insertVars(this.variablesString, values, view);

    let markup =  `
        ${this.header}
        <main class="flexbox-container flexbox-container--center-horizontal flexbox-container--vertical-start main-section main-section--profile main-section__about">
          ${view}
        </main>
      `;

    return container.html(markup).removeClass('view-wrapper--home view-wrapper--signup').addClass('view-wrapper--profile');
  }

  edit() {
    let container,
      editPath = this.path() + 'edit.html',
      markup,
      values = [this.user.name, this.user.email, this.user.webpage, this.user.phone, this.user.direction];

    if (this.helpers.getSize() == 'mobile') {
      container = $('.data-edit--mobile');
      this.oldAbout = container.html();
      markup = this.helpers.getRemote(editPath);
      markup = this.helpers.insertVars(this.variablesString, values, markup);

      return container.html(markup);
    } else {
      let target = $(window.event.target),
          fieldOldValue = target.data('value'),
          fieldPlaceholder = target.data('placeholder'),
          fieldName = target.data('name');

      target.addClass('active');
      target.parent().addClass('active');
      fieldOldValue = this.helpers.insertVars(this.variablesString, values, fieldOldValue);

      markup =  `
        <div class="form form--edit-desktop">
          <div class="form_arrow"></div>
          <div class="form__field field">
            <input type="text" class="form__field__input form__field__input--desktop-edit active" value="${fieldOldValue}">
            <label data-name="${fieldName}" class="form__field__title form__field__title--edit-desktop">${fieldPlaceholder}</label>
          </div>
          <button class="default-button default-button--form" id="cancel_save">Cancel</button>
          <button class="default-button default-button--form" id="save">Save</button>
        </div>
      `;

      container = target.parent();
      $('.form--edit-desktop').remove();

      return container.append(markup);
    }
    return container.html(markup);
  }

  cancelEdit() {
    if (this.helpers.getSize() == 'mobile') {
      return $('.data-edit--mobile').html(this.oldAbout);
    } else {
      let target = $(window.event.target);
      target.removeClass('active');
      target.parent().removeClass('active');
      return $('.form--edit-desktop').remove();
    }
  }

  saveEdit() {
    if (this.helpers.getSize() == 'mobile') {
      let userNew = JSON.stringify({
        email: $('#email').val(),
        name: $('#name').val(),
        direction: $('#direction').val(),
        phone: $('#phone').val(),
        webpage: $('#web-page').val()
      });

      let fieldName = ['email', 'name', 'direction', 'phone', 'webpage'];

      return this.database.update(this.user.id, userNew, fieldName);
    } else {
      let fieldName = $('.form__field__title').data('name').toLowerCase(),
          fieldValue = $('.form__field__input').val(),
          userNewField = JSON.stringify({
            [fieldName]: fieldValue
          });

      return this.database.update(this.user.id, userNewField, fieldName);
    }

  }

  logout() {
    $.session.set("username", null);
    $.session.clear();
    window.location.replace("");
  }

}
