export default class Header {

  constructor(view = 'login', user = 'na', helpers) {
    this.view = view;
    this.user = user;
    this.helpers = helpers;
    this.markup  =  '';
  }

  export() {
    switch(this.view) {
      case 'profile': {
        let markupUrl,
            varString = ['${this.user.name}', '${this.user.reviews}', '${this.user.phone}', '${this.user.direction}', '${this.user.followers}'],
            values = [this.user.name, this.user.reviews, this.user.phone, this.user.direction, this.user.followers];

        markupUrl = this.helpers.getPath() + 'header.html';
        this.markup = this.helpers.getRemote(markupUrl)
        this.markup = this.helpers.insertVars(varString, values, this.markup);
        return this.markup;
        break; 
      }
      case 'signup': { 
        return '<header class="header header--signup"><h1 class="title title--home">Signup</h1></header>';
        break; 
      } 
      default: { 
        return '<header class="header header--home"><h1 class="title title--home">Login</h1></header>';
        break; 
      }     
    }
  }
}