import Header from './header'

export default class Home { 

  constructor(database) {
    this.name = 'login';
    this.headerClass = new Header();
    this.header = this.headerClass.export();
    this.database = database;
  }


  show() {
    let container = $('.view-wrapper'),
        view;
    $.get('src/views/login/login.html', (data) => {
      view = data;
      let markup =  `
        ${this.header}
         <main class="flexbox-container flexbox-container--center-horizontal flexbox-container--vertical-start main-section--home">
            ${view}
         </main>
         <footer class="footer">
          <p class="text">MIT license - Alexander Pulido
          </p>
        </footer>
      `;
      return container.html(markup).removeClass('view-wrapper--home view-wrapper--profile').addClass('view-wrapper--signup');
    });
  }

  login() {
    let password = $('#password').val(),
        username = $('#user-name').val();
    this.database.find(username, password);
  }
}
