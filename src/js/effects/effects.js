// ===== Form Inputs =====



let input = '.form__field__input',
    placeholder = $('.placeholder'),
    parentInput = '.form__field',
    activeClass = 'active',
    _this;

$(document).on('click', parentInput, (e) => {
  $(e.target).addClass(activeClass); 
});

$(document).on('focus', input, (e) => {
  _this = $(e.target)
  _this.parents(parentInput).addClass(activeClass);
  _this.addClass(activeClass);
});

$(document).on('blur', input, () => {
  if (_this.val() == '') {
    _this.parents(parentInput).removeClass(activeClass);
    _this.removeClass(activeClass);
  }
});
